const Get = require('./tests/app/core/api/get.js');
const Post = require('./tests/app/core/api/postExecucao.js');
var resultado = ''

exports.config = {

    //------------------------------------------ Config gerais ------------------------------------------

    //highlightDelay: 100,
    //seleniumAddress: 'http://localhost:4444/wd/hub',

    AMBIENTE: process.env.AMB || "prod",
    CTID: process.env.CT_ID || "Default",
    EXECID: process.env.EXEC_ID || "Default",
    USER: process.env.CEAP_USER || '',
    PASS: process.env.CEAP_PASS || '',
    BASEURL: process.env.BASE_URL || 'https://me.miisy.com',
    APP: process.env.APP_NAME || 'bughunter',
    BUILD: process.env.BUILD || '',

    SQLEXEC: global.globalSqlExec = [],
    Data: global.globalData = new Map(),
    Fluxo: global.NomeFluxo = '',
    CTS: global.cts = [],

    restartBrowserBetweenTests: true,
    directConnect: true,
    framework: "jasmine2",
    specs: ['tests/executar/cliente/teste/test.spec.js'],

    //------------------------------------------ Preparando ambiente ------------------------------------------

    onPrepare: async function () {
        browser.ignoreSynchronization = true; // Se for true, não conecta na controller de Angula apps

        console.log('\n\n-------------------------------------------------------------------------------');
        console.log('\n\n                               --BUG-HUNTER--');
        console.log('\n\n-------------------------------------------------------------------------------');

        //Pegando o ID da execução
        let get = await Get('objects/execucao/', process.env.EXEC_ID);
        process.env.CT_ID = get.ids

        global.cts = (process.env.CT_ID).split(",");

        // pegando obj Caso de teste
        get = await Get('documents/casodeteste/', global.cts[0]);
        global.NomeFlux = get.casodeTeste.fluxo.nome;

        // Listener Report to CEAP
        var myReporter = {
            baseDirectory: 'target/testReport',

            jasmineStarted: function (suiteInfo) {
                console.log('\n\n                               --Iniciando suítes--');
                console.log('\n\nSuíte com total de cenários: ' + suiteInfo.totalSpecsDefined);
                console.log('-------------------------------------------------------------------------------');
            },

            suiteStarted: function (result) { },

            specStarted: function (result) {
                console.log('\n\n                               --Executando cenário--');
                console.log('\n\nExecutando CT ' + result.description);
                console.log('-------------------------------------------------------------------------------');
                resultado += result.description;
            },

            specDone: function (result) {
                console.log('\n-------------------------------------------------------------------------------');
                console.log('\n\n                               --Resultado do cenários--');
                console.log('\n\nCT ' + result.description + ' =====> ' + result.status);
                console.log('-------------------------------------------------------------------------------');
                resultado += ('£' + result.status);
                for (var i = 0; i < result.failedExpectations.length; i++) {
                    resultado += ('£' + result.failedExpectations[i].message);
                    // console.log('Falha: ' + result.failedExpectations[i].message);
                    // console.log(result.failedExpectations[i].stack);
                }
                //console.log(result.passedExpectations.length);
                resultado += '§';
            },

            suiteDone: function (result) {
                console.log('\n\n                               --Resultado final--');
                //console.log('\n\nSuite: ' + result.description + ' was ' + result.status);
                for (var i = 0; i < result.failedExpectations.length; i++) {
                    console.log('AfterAll ' + result.failedExpectations[i].message);
                    console.log(result.failedExpectations[i].stack);
                }

                // Escrevendo resultado no CEAP
                const post = Post(get, resultado);
            },

            jasmineDone: function () {
                console.log('-------------------------------------------------------------------------------');
                console.log('\n\n                               --Suíte finalizada--');
                console.log('\n\n-------------------------------------------------------------------------------');
            }
        };

        jasmine.getEnv().addReporter(myReporter);

        var AllureReporter = require('jasmine-allure-reporter');
        jasmine.getEnv().addReporter(new AllureReporter({
            resultsDir: 'target/allure-results'
        }));
    },

    //------------------------------------------ Browser ------------------------------------------

    capabilities: {
        browserName: 'chrome',
        ignoreProtectedModeSettings: true,
        chromeOptions: {
            args: [
                "--headless",
                "--disable-gpu",
                "--window-size=1920,1080",
                "--no-sandbox",
                "--disable-dev-shm-usage",
                "--disable-popup-blocking",
                "--disable-extensions",
                "--test-type"],
            prefs: {
                'download.default_directory': 'downloads',
                'download.prompt_for_download': "false",
                'download.directory_upgrade': "true",
                'Page.set_download_behavior': { 'behavior': 'allow' },
                //'Page.set_download_behavior': { 'behavior': 'allow', 'downloadPath': '${workspaceRoot}\\resource' },
            }
        }
    },

    //------------------------------------------ Jasmine ------------------------------------------

    jasmineNodeOpts: {
        showColors: true, // Use colors in the command line report.
        defaultTimeoutInterval: 180000,
        realtimeFailure: true
    }
}