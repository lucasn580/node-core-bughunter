####Base
FROM ubuntu:19.04

####Author
LABEL authors=Toshio

####Apps
RUN apt-get update

####Changing timezone
ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

####Expondo porta 4444
EXPOSE 4444

####Env
ENV IDs = ''
ENV user = ''
ENV pass = ''
ENV baseurl = ''
ENV app = ''
ENV build = '1'
#ENV AMB dev

####tools
RUN apt-get install -y apt-utils
RUN apt-get install -y xz-utils
RUN apt-get install -y apt-transport-https
RUN apt-get install -y software-properties-common
RUN apt-get install -y wget
RUN apt-get install -y curl

####java
RUN add-apt-repository ppa:openjdk-r/ppa
RUN apt-get update -q
RUN apt-get install -y openjdk-11-jdk

####Node Npm
RUN apt-get install -y nodejs
RUN apt-get install -y npm

####Chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - 
RUN sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update -y 
RUN apt-get install google-chrome-stable -y

####Add package to /app
ADD package.json /app/

####Entrando na pasta do sistema
WORKDIR /app/

####Atualizando Chrome
RUN apt-get update
RUN apt-get upgrade -y

####Clean
RUN apt-get autoremove -y
RUN apt-get autoclean -y
RUN rm -rf /var/lib/apt/lists/*

####Install some app
RUN npm update
RUN npm install
RUN npm i async -g
RUN npm i allure-commandline -g
RUN npm i jasmine -g
RUN npm i jasmine-allure-reporter -g
RUN npm i protractor -g
RUN npm i request -g
RUN npm i request-promise -g
RUN npm i tedious -g
RUN npm i webdriver-manager -g

####Copy code
COPY . /app/

####Run
#CMD EXEC_ID="${IDs}" CEAP_USER="${user}" CEAP_PASS="${pass}" BASE_URL="${baseurl}" APP_NAME="${app}" BUILD="${build}" npm test
#CMD ["/bin/bash"]