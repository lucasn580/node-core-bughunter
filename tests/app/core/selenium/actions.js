module.exports = async (objElement, myElement, passo, anexos) => {
    let texto = objElement.texto;
    let acao = objElement.acao.descricao + "(objElement, myElement, texto, passo, anexos)"

    try {

        // loga o quer irá reproduzir antes sem os passos;
        // console.log(`Ação que irá reproduzir: ${objElement.acao.descricao}`);

        await eval(acao);

    } catch (error) {

        print(`Falha na ação \'${objElement.acao.descricao}\'`, passo);

        globalData.set("semErro", false);

        fail(error);

    }
}



//------------------------------------------------ Acoes ------------------------------------------------



async function Clicar(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {
            console.log(`\npasso: ${passo} -> Clicando no elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await browser
                .wait(protractor.ExpectedConditions.elementToBeClickable(myElement), 30000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Clicando no elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await browser
                .wait(protractor.ExpectedConditions.visibilityOf(myElement), 30000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Clicando no elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await myElement.click();

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function Preencher(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Preenchendo o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await browser
                .wait(protractor.ExpectedConditions.visibilityOf(myElement), 30000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Preenchendo o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await browser
                .wait(protractor.ExpectedConditions.visibilityOf(myElement), 30000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Preenchendo o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            if (texto.includes("##"))
                texto = globalData.get(texto);

            await browser.executeScript(getElementJS(myElement, texto));

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function PreencherAutoComplete(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Preenchendo o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await browser
                .wait(protractor.ExpectedConditions.visibilityOf(myElement), 30000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Preenchendo o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await browser
                .wait(protractor.ExpectedConditions.visibilityOf(myElement), 30000,
                    `\n\n -> Falha no passo: ${passo}  - ID: ${globalData.get('IDAtual')} -> Preenchendo o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            if (texto.includes("##"))
                texto = globalData.get(texto);

            await myElement.sendKeys(texto, protractor.Key.ARROW_DOWN, protractor.Key.ENTER);

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function sendKeys(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Preenchendo o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await browser
                .wait(protractor.ExpectedConditions.visibilityOf(myElement), 30000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Preenchendo o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            if (texto.includes("##"))
                texto = globalData.get(texto);

            await myElement.sendKeys(texto);

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function Keys(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Preenchendo o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            const Keys = require('../../../app/core/selenium/keys.js');

            await Keys(myElement, texto);

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function Limpar(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Limpando o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await browser
                .wait(protractor.ExpectedConditions.visibilityOf(myElement), 30000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Limpando o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await myElement.clear();

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function Comparar(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            if (texto.includes("##"))
                texto = globalData.get(texto);

            let textoPego = await myElement.getText();

            console.log(`\npasso: ${passo} -> Comparando o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);
            console.log(`texto passado: \'${texto}\'`);
            console.log(`texto do elemento: \'${textoPego}\'`);

            await browser
                .wait(protractor.ExpectedConditions.visibilityOf(myElement), 30000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Comparando o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'\n texto: \'${texto}\'`);

            expect(texto).toEqual(textoPego);

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function Gravar(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Pegando elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await browser
                .wait(protractor.ExpectedConditions.visibilityOf(myElement), 30000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Pegando elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            myElement.getText().then((text) => {
                if (text == null)
                    throw new ActionException(`\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Pegando elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

                globalData.set(texto, text)

                console.log(`elemento gravado: \'${texto}\' = \'${globalData.get(texto)}\'`);
            })

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function Popup(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            await browser.getAllWindowHandles().then(async (handles) => {

                await browser.switchTo().window(handles[texto])
                    .then(() => {
                        console.log(`\npasso: ${passo} -> Trocando de Janela`);
                    });
            })

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function iframe(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Trocando de Frame: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            const locatorValue = (myElement.locator().value)

            if (locatorValue == "default")
                await browser
                    .switchTo()
                    .defaultContent();

            else
                await browser
                    .switchTo()
                    .frame(parseInt(locatorValue))

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function MoverMouse(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Movendo mouse para: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await browser
                .wait(protractor.ExpectedConditions.visibilityOf(myElement),
                    30000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Movendo mouse para: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await browser
                .actions()
                .mouseMove(myElement)
                .perform();

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function Screenshot(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            browser.sleep(1000);

            await print(texto, passo);

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function alertOk(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Alert OK`);

            await browser
                .wait(protractor.ExpectedConditions.alertIsPresent(),
                    3000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Alert OK`);

            await browser
                .switchTo()
                .alert()
                .accept();

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function alertCancelar(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Alert Cancelar`);

            await browser
                .wait(protractor.ExpectedConditions.alertIsPresent(),
                    3000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Alert Cancelar`);

            await browser
                .switchTo()
                .alert()
                .dismiss()

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function Esperar(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Esperando por: \'${texto}\'`);

            await browser.sleep(texto)

        } else fail(`Falha em alguma ação anterior`);
    }
}

// async function executarSQL(objElement, myElement, texto, passo, anexos) {
//     if (!await verificarIF()) {

//         if (globalData.get("semErro")) {

//             console.log(`\npasso: ${passo} -> Executando SQL: \'${nome}\'`);

//             const index = require('../../dao/index');
//             const nome = objElement.locatorvalue;

//             const data = await DLAnexo(anexos, nome)

//             if (texto.includes("##"))
//                 texto = globalData.get(texto);

//             var newValue = data.replace(/PARAMETRO/g, texto);

//             index(newValue);

//         } else fail(`Falha em alguma ação anterior`);
//     }
// }

async function MudarAquivo(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Mudando arquivo: \'${objElement.locatorvalue}\'`);

            const fs = require('fs');
            const nome = objElement.locatorvalue;
            const path = 'target/' + nome;

            const data = await DLAnexo(anexos, nome)

            if (texto.includes("##"))
                texto = globalData.get(texto);

            var newValue = data.replace(/PARAMETRO/g, texto);

            fs.writeFile(path, newValue, 'utf-8', (err) => {
                if (err) throw err;
            });

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function EnviarArquivo(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Preenchendo o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await browser
                .wait(protractor.ExpectedConditions.visibilityOf(myElement), 30000,
                    `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Preenchendo o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            await myElement.sendKeys(`${process.cwd()}/target/${texto}`);

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function AtualizarNavegador(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> Atualizando a tela`);

            await browser
                .driver
                .navigate()
                .refresh();

            browser.sleep(1000);

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function If(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            console.log(`\npasso: ${passo} -> If: Verificando a existencia do elemento \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

            if (texto) {

                let verificar = ((texto) == await myElement.getText());

                await browser.sleep(1);

                verificar ? globalData.set("DentroIF", true) : globalData.set("DentroIF", false);
            }

            else {

                let elementVisi = await browser
                    .wait(protractor.ExpectedConditions.visibilityOf(myElement),
                        30000,
                        `\n\n -> Falha no passo: ${passo} - ID: ${globalData.get('IDAtual')} -> Preenchendo o elemento: \'${objElement.elemento}\' Por: \'${myElement.locator().toString()}\'`);

                await browser.sleep(1);

                elementVisi ? globalData.set("DentroIF", true) : globalData.set("DentroIF", false);
            }

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function EndIf(objElement, myElement, texto, passo, anexos) {
    if (await verificarIF()) {

        if (globalData.get("semErro")) {

            await browser.sleep(5000);

            console.log(`\npasso: ${passo} -> EndIf: Terminando o \'If\'`);

            globalData.set("DentroIF", false);

        } else fail(`Falha em alguma ação anterior`);
    }
}

async function NumAleatorio(objElement, myElement, texto, passo, anexos) {
    if (!await verificarIF()) {

        if (globalData.get("semErro")) {

            let min = Math.ceil(99999999); // 7 casas
            let max = Math.floor(999999999); // 8 casas

            let rand = Math.floor(Math.random() * (max - min)) + min;

            console.log(`\npasso: ${passo} -> Gerando número aleatório: ${rand}`);

            globalData.set("##NUMALEATORIO", rand);

        } else fail(`Falha em alguma ação anterior`);
    }
}

//------------------------------------------------ OUTROS ------------------------------------------------



async function verificarIF() {
    await browser.sleep(1);

    return globalData.get("DentroIF");
}

async function print(texto, passo) {

    if (globalData.get("semErro")) {

        await browser.takeScreenshot().then((png) => {
            allure.createAttachment('Screenshot', () => {
                return new Buffer(png, 'base64')
            }, 'image/png')();
        });

        let name = `CT_ID: ${globalData.get('IDAtual')} - passo ${passo} - ${texto}`;

        console.log(`\npasso: ${passo} -> ${name} - Tirando screenshot`);

    } else fail(`Falha em alguma ação anterior`);
}

function getElementJS(element, texto) {
    const locator = element.locator().toString();

    if (locator.includes("css")) {
        return 'document.querySelector("' + element.locator().value + '").setAttribute("value", "' + texto + '")';

    } else if (locator.includes("className")) {
        return 'document.getElementsByClassName("' + element.locator().value + '").value = ' + texto + '"';

    } else if (locator.includes("id")) {
        return 'document.getElementById("' + element.locator().value + '").value = ' + texto + '"';

    } else if (locator.includes("name")) {
        return 'document.getElementsByName("' + element.locator().value + '").value = ' + texto + '"';

    } else if (locator.includes("tagName")) {
        return 'document.getElementsByTagName("' + element.locator().value + '").value = ' + texto + '"';

    } else if (locator.includes("xpath")) {
        return 'document.evaluate("' + element.locator().value + '", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value = ' + texto + '"';
    }
}

function ActionException(message) {

    this.name = "ActionException";

    this.message = '\n\n-------------------------------------------------------------------------------';
    this.message = this.message + "\n" + message;
    this.message = this.message + '\n\n-------------------------------------------------------------------------------';

    throw new Error(this.message);
}

function DLAnexo(anexos, nome) {
    if (anexos.length) {
        const DL = require('../api/getDownload');

        // salvanco os anexos
        let anexoToDl = anexos.filter(anexo => anexo.name === nome);

        return DL(anexoToDl[0].url);
    }
}