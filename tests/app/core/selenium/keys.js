module.exports = async (myElement, texto) => {

    //link para todas as teclas
    //https://gist.github.com/boxmein/f86b6e196d3c0c472f302c80f7388704


    texto = texto.toUpperCase();

    let keys = (texto).split(",");

    keys.forEach(async key => {

        await browser.sleep(100);

        console.log(key);

        switch (key) {

            case "ADD":
                await myElement.sendKeys(protractor.Key.ADD);
                break;

            case "ALT":
                await myElement.sendKeys(protractor.Key.ALT);
                break;

            case "ARROW_DOWN":
                await myElement.sendKeys(protractor.Key.ARROW_DOWN);
                break;

            case "ARROW_LEFT":
                await myElement.sendKeys(protractor.Key.ARROW_LEFT);
                break;

            case "ARROW_RIGHT":
                await myElement.sendKeys(protractor.Key.ARROW_RIGHT);
                break;

            case "ARROW_UP":
                await myElement.sendKeys(protractor.Key.ARROW_UP);
                break;

            case "BACK_SPACE":
                await myElement.sendKeys(protractor.Key.BACK_SPACE);
                break;

            case "CANCEL":
                await myElement.sendKeys(protractor.Key.CANCEL);
                break;

            case "CLEAR":
                await myElement.sendKeys(protractor.Key.CLEAR);
                break;

            case "COMMAND":
                await myElement.sendKeys(protractor.Key.COMMAND);
                break;

            case "CONTROL":
                await myElement.sendKeys(protractor.Key.CONTROL);
                break;

            case "DECIMAL":
                await myElement.sendKeys(protractor.Key.DECIMAL);
                break;

            case "DELETE":
                await myElement.sendKeys(protractor.Key.DELETE);
                break;

            case "DIVIDE":
                await myElement.sendKeys(protractor.Key.DIVIDE)
                break;

            case "DOWN":
                await myElement.sendKeys(protractor.Key.DOWN)
                break;

            case "END":
                await myElement.sendKeys(protractor.Key.END)
                break;

            case "ENTER":
                await myElement.sendKeys(protractor.Key.ENTER)
                break;

            case "EQUALS":
                await myElement.sendKeys(protractor.Key.EQUALS)
                break;

            case "ESCAPE":
                await myElement.sendKeys(protractor.Key.ESCAPE)
                break;

            case "F1":
                await myElement.sendKeys(protractor.Key.F1)
                break;

            case "F10":
                await myElement.sendKeys(protractor.Key.F10)
                break;

            case "F11":
                await myElement.sendKeys(protractor.Key.F11)
                break;

            case "F12":
                await myElement.sendKeys(protractor.Key.F12)
                break;

            case "F2":
                await myElement.sendKeys(protractor.Key.F2)
                break;

            case "F3":
                await myElement.sendKeys(protractor.Key.F3)
                break;

            case "F4":
                await myElement.sendKeys(protractor.Key.F4)
                break;

            case "F5":
                await myElement.sendKeys(protractor.Key.F5)
                break;

            case "F6":
                await myElement.sendKeys(protractor.Key.F6)
                break;

            case "F7":
                await myElement.sendKeys(protractor.Key.F7)
                break;

            case "F8":
                await myElement.sendKeys(protractor.Key.F8)
                break;

            case "F9":
                await myElement.sendKeys(protractor.Key.F9)
                break;

            case "HELP":
                await myElement.sendKeys(protractor.Key.HELP)
                break;

            case "HOME":
                await myElement.sendKeys(protractor.Key.HOME)
                break;

            case "INSERT":
                await myElement.sendKeys(protractor.Key.INSERT)
                break;

            case "LEFT":
                await myElement.sendKeys(protractor.Key.LEFT)
                break;

            case "META":
                await myElement.sendKeys(protractor.Key.META)
                break;

            case "MULTIPLY":
                await myElement.sendKeys(protractor.Key.MULTIPLY)
                break;

            case "NULL":
                await myElement.sendKeys(protractor.Key.NULL)
                break;

            case "NUMPAD0":
                await myElement.sendKeys(protractor.Key.NUMPAD0)
                break;

            case "NUMPAD1":
                await myElement.sendKeys(protractor.Key.NUMPAD1)
                break;

            case "NUMPAD2":
                await myElement.sendKeys(protractor.Key.NUMPAD2)
                break;

            case "NUMPAD3":
                await myElement.sendKeys(protractor.Key.NUMPAD3)
                break;

            case "NUMPAD4":
                await myElement.sendKeys(protractor.Key.NUMPAD4)
                break;

            case "NUMPAD5":
                await myElement.sendKeys(protractor.Key.NUMPAD5)
                break;

            case "NUMPAD6":
                await myElement.sendKeys(protractor.Key.NUMPAD6)
                break;

            case "NUMPAD7":
                await myElement.sendKeys(protractor.Key.NUMPAD7)
                break;

            case "NUMPAD8":
                await myElement.sendKeys(protractor.Key.NUMPAD8)
                break;

            case "NUMPAD9":
                await myElement.sendKeys(protractor.Key.NUMPAD9)
                break;

            case "PAGE_DOWN":
                await myElement.sendKeys(protractor.Key.PAGE_DOWN)
                break;

            case "PAGE_UP":
                await myElement.sendKeys(protractor.Key.PAGE_UP)
                break;

            case "PAUSE":
                await myElement.sendKeys(protractor.Key.PAUSE)
                break;

            case "RETURN":
                await myElement.sendKeys(protractor.Key.RETURN)
                break;

            case "RIGHT":
                await myElement.sendKeys(protractor.Key.RIGHT)
                break;

            case "SEMICOLON":
                await myElement.sendKeys(protractor.Key.SEMICOLON)
                break;

            case "SEPARATOR":
                await myElement.sendKeys(protractor.Key.SEPARATOR)
                break;

            case "SHIFT":
                await myElement.sendKeys(protractor.Key.SHIFT)
                break;

            case "SPACE":
                await myElement.sendKeys(protractor.Key.SPACE)
                break;

            case "SUBTRACT":
                await myElement.sendKeys(protractor.Key.SUBTRACT)
                break;

            case "TAB":
                await myElement.sendKeys(protractor.Key.TAB)
                break;

            case "UP":
                await myElement.sendKeys(protractor.Key.UP)
                break;
        }
    });
}