
/**
 * Retorna o Elemento
 * 
 * tipos de seletor que podem ser utilizados:
 * 
 * Function	        Description
 * 
 * className	    Locates elements that have a specific class name.
 * css	            Locates elements using a CSS selector.
 * id	            Locates an element by its ID.
 * linkText	        Locates link elements whose visible text matches the given string.
 * js	            Locates an elements by evaluating a JavaScript expression, which may be either a function or a string.
 * name	            Locates elements whose name attribute has the given value.
 * partialLinkText	Locates link elements whose visible text contains the given substring.
 * tagName	        Locates elements with a given tag name.
 * xpath	        Locates elements matching a XPath selector.
 * 
 */
module.exports = function (tipoSelector, locator) {

    switch (tipoSelector) {

        case "className":
            return className(locator);

        case "cssSelector":
            return cssSelector(locator);

        case "id":
            return id(locator);

        case "linkText":
            return linkText(locator);

        case "js":
            return js(locator);

        case "name":
            return name(locator);

        case "partialLinkText":
            return partialLinkText(locator);

        case "tagName":
            return tagName(locator);

        case "xpath":
            return xpath(locator);
    }
}

/**
 * Retorna o elemento localizado pelo className
 * @param {Localizador do elemento} locator 
 */
function className(locator) {
    return protractor.element(by.className(locator));
}

/**
 * Retorna o elemento localizado pelo css
 * @param {String do Localizador do elemento} locator 
 */
function cssSelector(locator) {
    return protractor.element(by.css(locator));
}

/**
 * Retorna o elemento localizado pelo id
 * @param {String do Localizador do elemento} locator 
 */
function id(locator) {
    return protractor.element(by.id(locator));
}

/**
 * Retorna o elemento localizado pelo linkText
 * @param {String do Localizador do elemento} locator 
 */
function linkText(locator) {
    return protractor.element(by.linkText(locator));
}

/**
 * Retorna o elemento localizado pelo js
 * @param {String do Localizador do elemento} locator 
 */
function js(locator) {
    return protractor.element(by.js(locator));
}

/**
 * Retorna o elemento localizado pelo name
 * @param {String do Localizador do elemento} locator 
 */
function name(locator) {
    return protractor.element(by.name(locator));
}

/**
 * Retorna o elemento localizado pelo partialLinkText
 * @param {String do Localizador do elemento} locator 
 */
function partialLinkText(locator) {
    return protractor.element(by.partialLinkText(locator));
}

/**
 * Retorna o elemento localizado pelo tagName
 * @param {String do Localizador do elemento} locator 
 */
function tagName(locator) {
    return protractor.element(by.tagName(locator));
}

/**
 * Retorna o elemento localizado pelo xpath
 * @param {String do Localizador do elemento} locator 
 */
function xpath(locator) {
    return protractor.element(by.xpath(locator));
}