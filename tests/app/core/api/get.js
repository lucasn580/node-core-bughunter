var request = require('request-promise');

module.exports = function (PATH, PARAM) {
    const
        user = process.env.CEAP_USER,
        pass = process.env.CEAP_PASS,
        autho = "Basic " + new Buffer.from(user + ":" + pass).toString("base64"),
        url = process.env.BASE_URL + '/api/' + process.env.APP_NAME + '/' + PATH + PARAM,
        options = {
            url: url,
            headers: {
                "content-type": "application/json",
                "Authorization": autho
            },
            json: true
        }

    return request(options, (error, response, body) => {
        // console.log('\n\n                               --Buscando--');
        // console.log("\n\nLocal: " + url);
        // console.log("Status code: ", response && response.statusCode); // Printa o status recebido pelo Response
        // console.log('body ' + body);
        
        if (error)
            console.log("error: ", error); // Printa erro se tiver

        // console.log("-------------------------------------------------------------------------------");
    });
}