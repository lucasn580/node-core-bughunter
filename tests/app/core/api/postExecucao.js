var request = require('request-promise');

module.exports = function (exec, result) {
    const
        user = process.env.CEAP_USER,
        pass = process.env.CEAP_PASS,
        uri = process.env.BASE_URL + '/api/' + process.env.APP_NAME + '/objects/execucao/' + exec.id,
        autho = "Basic " + new Buffer.from(user + ":" + pass).toString("base64"),
        options = {
            method: 'POST',
            uri: uri,
            headers: {
                "content-type": "application/json",
                "Authorization": autho
            },
            body: {
                "data": exec.data,
                "ids": exec.ids,
                "resultado": result,
                "usuario": exec.usuario,
                "report": `http://10.11.9.3:8080/job/Teste/${process.env.BUILD}/allure/`
            },
            json: true
        }

    return request(options, (error, response, body) => {
        console.log("\n\n                               --Enviando resultados--");
        console.log("\n\nStatus code:", response && response.statusCode); // Printa o status recebido pelo Response
        console.log(options.body.resultado);


        if (error != null)
            console.log("error:", error); // Printa erro se tiver

        console.log("-------------------------------------------------------------------------------\n");
    });
}