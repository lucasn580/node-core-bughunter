var request = require('request-promise');

module.exports = function (PATH) {
    const
        user = process.env.CEAP_USER,
        pass = process.env.CEAP_PASS,
        autho = "Basic " + new Buffer.from(user + ":" + pass).toString("base64"),
        url = process.env.BASE_URL + PATH,
        options = {
            url: url,
            headers: {
                "content-type": "application/json",
                "Authorization": autho
            },
            json: true
        }

    return request(options, (error, response, body) => {
        // console.log('\n\n                               --Buscando--');
        // console.log("\n\nLocal: " + PATH);
        // console.log("Status code: ", response && response.statusCode); // Printa o status recebido pelo Response

        if (error != null)
            console.log("error: ", error); // Printa erro se tiver

        // console.log("-------------------------------------------------------------------------------");
    });
}