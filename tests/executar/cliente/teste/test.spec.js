const Get = require('../../../app/core/api/get');
const Element = require('../../../app/core/selenium/element');
const Actions = require('../../../app/core/selenium/actions');
const fs = require('fs');

//------------------------------------------ CT ------------------------------------------
describe(global.NomeFlux, () => {
    //Variaveis globais para verificar if e se teve erro
    globalData.set("DentroIF", false);
    globalData.set("semErro", true);

    //forEach para rodar cada um dos cts que virem das execuções
    global.cts.forEach(ct => {
        const dir = "target/";

        //verificando e criando a pasta target
        if (!fs.existsSync(dir))
            fs.mkdirSync(dir);

        //Rodando os teste
        it('ID: ' + ct, async () => {
            let passo = 1;
            let anexos = "";

            // pegando obj Caso de teste
            const get = await Get('documents/casodeteste/', ct);

            // Pegando o ID atual da execução
            globalData.set("IDAtual", ct);

            // pegando os anexos
            if (get.casodeTeste.anexo)
                anexos = get.casodeTeste.anexo.files;

            // pegando a lista de obj
            let listaObjs = get.casodeTeste.pageObjectsList;

            // Verificando se tem passos para serem executados
            if (listaObjs.length != 0) {
                console.log("\n\n                               --Iniciando cenários--");
                console.log("\n\nPassos a reporduzir");

                // entrando na página inicial
                browser.get(listaObjs[0].pagina);

                // ordenando obj null
                retirarNullObjs(listaObjs);

                // ordenando Novamente os passos sem os null
                listaObjs = ordenarLista(listaObjs);

                // executando passos
                listaObjs.forEach(elements => {

                    // pega os elementos
                    let myElement = Element(elements.locator.locatorName, elements.locatorvalue);

                    // executa as ações
                    Actions(elements, myElement, passo++, anexos)

                });

            } else
                console.log("Não possuem passos à serem executados... \nParando a execução.");
        });
    });

    //------------------------------------------ Outros metodos ------------------------------------------

    /**
     * Ordena os objetos para executar
     * @param {lista de Objetos para colocar em ordem de execução} listaObjs 
     */
    function ordenarLista(listaObjs) {
        let myArray = [];
        let contador = 0;
        const qtd = listaObjs.length;

        while (qtd >= contador++) {
            listaObjs.forEach(elements => {
                if (elements.ordem == contador)
                    myArray.push(elements);
            })
        }
        return myArray;
    }

    /**
     * retira os elementos null poem ele em uma ordem
     * @param {Lista de elementos para ordenar} listaObjs 
     */
    function retirarNullObjs(listaObjs) {
        // lista completa
        listaObjs.forEach(objOrdem => {
            let cont = 1;

            // pegando os null
            if (objOrdem.ordem == null) {

                // lista completa
                listaObjs.forEach(element => {

                    // retirando os null
                    if (element.ordem != null)

                        // verificando se a lista sem null tem um lugar para se encaixar na lista
                        if (element.ordem != cont)
                            objOrdem.ordem = cont++;
                })
            }
        });
    }

    //------------------------------------------ Iniciais ------------------------------------------
    beforeEach(() => {
        browser.waitForAngularEnabled(false);
    })

    afterEach(() => {
        browser.sleep(1000);
    });
});